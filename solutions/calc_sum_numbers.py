def calc_sum_numbers(number):
    sum = 0
    while number > 0:
        sum = sum + number
        number = number - 1
    return sum


print(calc_sum_numbers(4))
