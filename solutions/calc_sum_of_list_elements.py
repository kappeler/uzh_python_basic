def calc_sum_of_list_elements(numbers):
    index = 0
    result = 0
    while index < len(numbers):
        number = numbers[index]
        result = result + number
        index = index + 1
    return result


my_number_list = [1, 7, 3, 5]
print(calc_sum_of_list_elements(my_number_list))
