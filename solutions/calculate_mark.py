def calculate_mark(points, max_points):
    points = float(points)
    max_points = float(max_points)
    mark = points / max_points * 5 + 1
    rounded_mark = round(mark / 0.5) * 0.5
    return rounded_mark


print(calculate_mark(85, 100))
print(calculate_mark('85', '100'))
