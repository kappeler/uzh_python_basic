def count_words(words, word_length):
    result = 0
    for word in words:
        if len(word) >= word_length:
            result = result + 1
    return result


words = ['Alfred', 'Anna', 'Peter', 'Albert', 'Al']
print(count_words(words, 5))
