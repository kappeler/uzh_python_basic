import math


def distance(x1, y1, x2, y2):
    root_expression = (x2 - x1) ** 2 + (y2 - y1) ** 2
    result = math.sqrt(root_expression)
    return result


print(distance(0, 0, 1, 0))
print(distance(3, 10, 5, 3))
