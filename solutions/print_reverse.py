def print_reverse(text):
    index = -1
    reversed_string = ""
    while index >= -len(text):
        reversed_string = reversed_string +  text[index]
        index = index - 1
    return reversed_string


print(print_reverse('Hello my friends'))


def print_reverse(text):
    index = len(text) - 1
    reversed_string = ""
    while index >= 0:
        reversed_string = reversed_string + text[index]
        index = index - 1
    return reversed_string


print(print_reverse('Hello my friends'))


text = 'Hello my friends'
print(text[::-1])
