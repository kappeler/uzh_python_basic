def calculate_mark(points, max_points):
    points = float(points)
    max_points = float(max_points)
    mark = points / max_points * 5 + 1
    rounded_mark = round(mark / 0.5) * 0.5
    return rounded_mark


while True:
    points = input("Please enter the points achieved at the exam: ")
    if points == 'exit':
        break

    max_points = input("Please enter the max points possible: ")

    print(calculate_mark(points, max_points))
