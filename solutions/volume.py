import math


def volume_from_radius(radius):
    result = 4 / 3 * math.pi * radius ** 3
    return result


print(volume_from_radius(1))
