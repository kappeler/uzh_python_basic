import math


def distance(x1, y1, x2, y2):
    root_expression = (x2 - x1) ** 2 + (y2 - y1) ** 2
    result = math.sqrt(root_expression)
    return result


def volume_from_radius(radius):
    result = 4 / 3 * math.pi * radius ** 3
    return result


def volume_from_points(x1, y1, x2, y2):
    point_distance = distance(x1, y1, x2, y2)
    result = volume_from_radius(point_distance)
    return result


print(volume_from_points(0, 0, 0, 1))
